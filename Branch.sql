CREATE TABLE Branch(
    Branch_number INT PRIMARY KEY,
    Branch_Addr varchar(50) NOT NULL
);

CREATE TABLE Book(
    ISBN INT PRIMARY KEY,
    Title VARCHAR(30) NOT NULL,
    Author VARCHAR(20) NOT NULL,
    Publisher VARCHAR(20) NOT NULL
    Branch_number INT NOT NULL,
    FOREIGN KEY (Branch_number) REFERENCES Branch(Branch_number)
);

CREATE TABLE Invetory(
    ISBN INT PRIMARY KEY,
    Num_copies INT NOT NULL
    FOREIGN KEY(ISBN) REFERENCES Book(ISBN)
);