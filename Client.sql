CREATE TABLE Client(
    Client_number INT PRIMARY KEY,
    Name VARCHAR(20) NOT NULL,
    Location VARCHAR(10) NOT NULL,
    Manager_number INT
);

CREATE TABLE Manager(
    Manager_number INT PRIMARY KEY,
    Manager_name VARCHAR(20) NOT NULL,
    Manager_location VARCHAR(10) NOT NULL
    FOREIGN KEY(Manager_number) REFERENCES Client(Manager_number)
);

CREATE TABLE Contract(
    Contract_number INT PRIMARY,
    Estimated_cost INT NOT NULL,
    Client_number INT NOT NULL
    Completion_date DATETIME
    FOREIGN KEY(Client_number) REFERENCES Client(Client_number)
);

CREATE TABLE Staff(
    Staff_number INT PRIMARY KEY,
    Staff_name  VARCHAR(20) NOT NULL,
    Staff_location TEXT NOT NULL
    Contract_number INT NOT NULL,
    FOREIGN KEY (Contract_number) REFERENCES Contract(Contract_number)
);