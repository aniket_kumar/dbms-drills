CREATE TABLE Doctor(
    Doctor_number INT PRIMARY KEY,
    DoctorName VARCHAR(20) NOT NULL,
    Secretary VARCHAR(20) NOT NULL
);

CREATE TABLE Patient(
    Patient_number INT PRIMARY KEY
    PatientName VARCHAR(20) NOT NULL,
    PatientDOB DATE NOT NULL,
    PatientAddress VARCHAR(30) NOT NULL,
    Prescription_number INT NOT NULL
    Doctor_number INT NOT NULL,
    FOREIGN KEY(Doctor_number) REFERENCES Doctor(Doctor_number)
);

CREATE TABLE Prescription(
    Prescription_number INT PRIMARY KEY,
    Drug VARCHAR(30) NOT NULL,
    Date DATE NOT NULL,
    Dosage INT NOT NULL
    FOREIGN KEY(Prescription_number) REFERENCES Patient(Prescription_number)
);