CREATE TABLE Patient(
    Patient_number INT PRIMARY KEY,
    Name VARCHAR(20) NOT NULL,
    DOB DATE NOT NULL,
    Address VARCHAR(30) NOT NULL
    Prescription_number INT NOT NULL
);

CREATE TABLE Prescription(
    Prescription_number INT PRIMARY KEY,
    Drug VARCHAR(30) NOT NULL,
    Date DATE NOT NULL,
    Dosage INT NOT NULL,
    Doctor_ID INT NOT NULL
    FOREIGN KEY (Prescription_number) REFERENCES Patient(Prescription_number) 
);

CREATE TABLE Doctor(
    Doctor_ID INT NOT NULL,
    Doctor VARCHAR(20) NOT NULL,
    Secretary VARCHAR(20) NOT NULL
    FOREIGN KEY(Doctor_ID) REFERENCES Prescription(Doctor_ID)
);